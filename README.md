# Onboarding - Release Stage

This project helps the Release stage onboard its new members.
An [issue template](.gitlab/issue_templates/Release_Onboarding.md) is used to create onboarding issues which supplement the [general onboarding](https://gitlab.com/gitlab-com/people-ops/employment/issues) issues. 
